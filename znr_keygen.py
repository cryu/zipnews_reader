#! /usr/bin/env python3

# This encrypts/decrypts various bits for Zipnews Reader
#
# Known keys:
#       "jaxsoft per bill" (znreader.key, key found in ZNR.EXE)
#       "Uqwk does ZipNews. Hooray." (${USERID}.ID, key found in uqwk's reply.c)

import argparse
import random
import textwrap

def transmogrify(incoming, key_string):
    out = bytearray()
    key = [ord(x) for x in list(key_string)]
    key_index = 0
    
    for byte in incoming:
        if type(byte) is not int:
            byte = ord(byte)        
        out.append( (byte ^ key[key_index] ^ (key[0] * key_index) ) & 0xff)
        key[key_index] = (key[key_index] + key[(key_index + 1) % len(key)]) & 0xff
        if key[key_index] == 0:    # this doesn't seem to ever happen
            key[key_index] = 1
        key_index = (key_index + 1) % len(key)
    return out

def main():
    parser = argparse.ArgumentParser(
        prog='znr_keygen.py',
        description='Generate registration key for ZipNews Reader (version 0.3)',
        epilog='Written by Chris RYU (https://codeberg.org/cryu/zipnews_reader)'
        )

    parser.add_argument('-o', '--outfile', type=str, dest='outfile', help='output file (default "znreader.key")', default='znreader.key')
    parser.add_argument('-k', '--key', type=str, dest='key', help='encryption key (default "jaxsoft per bill")', default='jaxsoft per bill')
    parser.add_argument('-s', '--serial', type=str, dest='serial', help='serial number (default random integer)', default=str(random.randrange(99999)))
    parser.add_argument('-u', '--username', type=str, dest='username', help='username (default "Henry Dorsett Case")', default='Henry Dorsett Case')
    parser.add_argument('-1', '--account1', type=str, dest='a1', help='Account 1 (default "account1")', default='account1')
    parser.add_argument('-2', '--account2', type=str, dest='a2', help='Account 2 (default "account2")', default='account2')
    parser.add_argument('-3', '--account3', type=str, dest='a3', help='Account 3 (default "account3")', default='account3')
    parser.add_argument('-r', '--read', type=str, dest='readfile', help='decode keyfile (default "")', default='')
    args = parser.parse_args()

    if len(args.readfile) > 0:
        with open(args.readfile, 'rb') as file:
            ciphertext = file.read()
            outdata = transmogrify(ciphertext, args.key)
            try:
                print(outdata.decode('ASCII'), end='')
            except:
                # if ASCII print fails, then write to znreader.key
                writekey(args.outfile, outdata)
    else:
        cleartext = "ZipNews Reader #{}\n{}\n{}\n{}\n{}\n".format(args.serial, args.username, args.a1, args.a2, args.a3)

        print("znr_keygen.py: generating key with payload:")
        print(textwrap.indent(cleartext, '\t'))

        generated_key = transmogrify(cleartext, args.key)
        writekey(args.outfile, generated_key)

def writekey(file, data):
    with open(file, "wb") as binary_file:
        binary_file.write(data)
    print("znr_keygen.py: wrote keyfile to {}".format(file))

if __name__=='__main__':
    main()
