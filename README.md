![ZipNewsReader](znr.png?raw=true "ZipNews Reader")

# ZipNews Reader

## What?

From the release announcements posted to comp.os.msdos.mail-news:

> ZipNews Reader is the DOS-based, QWK-like offline reader for Internet/Usenet use. It is NOT QWK-compatible, but IS significantly better. A key design objective is compliance with the Internet message and news standards, RFC-822 and RFC-1036, so that normal Internet mail options and capabilities are available to its users.

From "About ZipNews":

> The ZipNews Reader/Mailer is a threaded newsreader for the Usenet newsgroups available via the Internet.  It is the companion program to the ZipNews Door for PCBoard, Wildcat!, Waffle, and DOOR.SYS-compatible BBSs.  It also interfaces with Uqwk v1.7, and later, on Unix hosts.  Unix host users may get uqwk via anon FTP: ftp.gte.com:/pub/uqwk/uqwk1.7.tar.Z
>
> The ZipNews reader was originally released as a threaded news reader and had no capabilities for handling Internet E-mail or uploads back to the ZipNews Door.  Such capabilities were added later for use in only registered versions of the reader.  The reader continues to grow in capability, support via the Internet, and in numbers of happy users.
>
> The registration fee for the ZipNews Reader/Mailer is $19.95 (U.S.), payable by check to:  J. C. Kilday Associates, P.O. Box 1961, Portland, Maine 04104, USA.  The developer may be reached at: jkilday@nlbbs.com 

That email address is defunct, of course, and Jack isn't accepting money for ZipNews registrations.  Use the included key generator instead.

## Why?

Due to retirement, the ensuing boredom, and overwhelming nostalgia, I've been putting together replicas of the machines I used in the 80s/90s.

One of the bits of software I used quite a bit was a pretty neat offline USENET reader named "ZipNews Reader". It was written by Jack Kilday (who ran the Northern Lights BBS in Portland, ME), and was available in a demo version on SIMTEL.

A downside of ZNR was that it was crippled unless it was registered (no posting, no email). I lost my registration key years back, but I managed to reverse-engineer the key format and encryption algorithm (with the help of IDA, dosbox, and an obfuscated-and-somewhat-buggy C implementation in uqwk) and put together a key generator.

Jack is long-retired, but has graciously given his approval for this project.  Per his son Mike (taoteh1221@gmail.com):

> Dad gives his permission for Chris to do whatever he wants with ZipNews as long as he is not contacted or emailed.

... so please don't contact Jack about anything in this archive; contact me instead.

## How?

First, generate a registration key using the included ```znr_keygen.py```.  If no command-line options are given, it will create a key for user "Henry Dorsett Case", with three accounts ("account1", "account2", and "account3") and save it as "znreader.key" in the current directory.  Use ```-h```/```--help``` for key customization information.

We used to use "uqwk" to process packets to and from ZipNews Reader.  I've re-implemented the necessary functionality in "uznr", a Python program located in the "uznr" subdirectory of this repository.

Build it with "python -m build", install with "pip install dist/uznr-0.2-py3-none-any.whl", execute "uznr --help" for usage information.

## Notes?

* The only apparent differences between choosing "UQWK" instead of "ZIPNEWS" in "Setup/Config Changes/Utility/Packer" is the key used to encrypt ```${USERNAME}.ID``` (and the .ID file starts with "ZNR+UQWK" instead of just "ZNR").  The contents of ```${USERNAME}.ID``` are otherwise identical, and uznr will attempt decryption with both known keys.
* The key generator can also decode keys (it's a symmetric cipher with a fixed key and a known-plaintext preamble), so if you still have a key lying around, you can decode, alter, and re-encode it. (And please do let me know if the format of the key differs from that of the key generator, so I can correct any issues)

## Known releases (* denotes an unarchived release):
- v0.93b    1994/07/21
- v0.93a    1994/07/03
- v0.92y    1994/03/28
- v0.92x    1994/01/25
- v0.92w    1993/12/25
- v0.92v*   1993/12/15
- v0.92u    1993/12/05
- v0.92t*   1993/11/27
- v0.92s    1993/11/08
- v0.92r*   1993/10/30
- v0.92q*   (No public release)
- v0.92p*   1993/10/18
- v0.92n    1993/09/29
- v0.92m*   1993/09/08
- v0.92k*   1993/08/08
- v0.92j*   1993/08/01
- v0.92i    1993/07/31
- v0.92h*   1993/07/30
- v0.92g*   1993/06/26
- v0.92.fa* 1993/06/23
- v0.92f*   1993/06/20      
- v0.92e    1993/05/15
- v0.92d    1993/03/27
- v0.92c*   1993/03/24
- v0.92b*   1993/03/07
- v0.92a*   1993/03/05
- v0.92*    1993/03/04
- v0.91g*   1993/03/01
- v0.91f*   1993/02/28
- v0.91e*   1993/02/26
- v0.91d*   1993/02/25
- v0.91c*   1993/02/23
