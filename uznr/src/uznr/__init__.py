#! /usr/bin/env python3

import os
import argparse
import sys
import re
import nntplib
import zipfile
import fnmatch
import email
import smtplib
import tempfile
import textwrap
import chardet
from email.header import decode_header as _email_decode_header
from unidecode import unidecode
from shutil import rmtree

def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)

class ZNR:
    nntp_server = 'localhost'
    smtp_server = 'localhost'
    user_name = os.environ['USER']
    host_name = 'localhost'
    newsrc_filename = None
    nws_filename = None
    idx_filename = None
    join_filename = None
    packet_filename = None
    group_data = []
    newsrc = []
    blk_cnt = 0
    max_blks = 0
    nntp = None
    zip_flag = False
    end_offset = 0
    article_count = 0
    tmpdir = None
    keys = [ 'Uqwk does ZipNews. Hooray.', 'jaxsoft per bill' ]
    from_address = None
    from_gecos = None
    output_file = ''

    def __init__(self, smtp_server, nntp_server, from_address, from_gecos, output_file):
        self.newsrc_filename = os.environ['HOME']+'/.newsrc-znr'
        self.tmpdir = tempfile.mkdtemp()
        self.smtp_server = smtp_server
        self.nntp_server = nntp_server
        self.from_address = from_address
        self.from_gecos = from_gecos
        self.output_file = output_file
        self.nws_filename = '{}/{}.nws'.format(self.tmpdir, self.user_name)
        self.idx_filename = '{}/{}.idx'.format(self.tmpdir, self.user_name)
        self.join_filename = '{}/{}.jn'.format(self.tmpdir, self.user_name)
        for file in [ self.nws_filename, self.idx_filename, self.join_filename]:
            f = open(file, "ab")
            f.truncate(0)
            f.close()

    def DoZipfile(self):
        with zipfile.ZipFile(self.output_file, mode='w', compresslevel=9, compression=zipfile.ZIP_DEFLATED) as myzip:
            myzip.write(self.nws_filename, arcname=os.path.basename(self.nws_filename))
            myzip.write(self.idx_filename, arcname=os.path.basename(self.idx_filename))
            myzip.write(self.join_filename, arcname=os.path.basename(self.join_filename))

    # borrowed from https://stackoverflow.com/questions/43788106/convert-a-list-of-numbers-to-ranges
    def ranges(self, data):
        result = []
        if not data:
            return result
        idata = iter(data)
        first = prev = next(idata)
        for following in idata:
            if following - prev == 1:
                prev = following
            else:
                result.append((first, prev + 1))
                first = prev = following
        # There was either exactly 1 element and the loop never ran,
        # or the loop just normally ended and we need to account
        # for the last remaining range.
        result.append((first, prev+1))
        return result

    # Parse newsrc, create array of read articles per group
    def SubList (self, newsrc_line):
        sub_list = []
        res = []

        # through line entries
        entry = list(filter(None, newsrc_line.split(','))) # remove empties
        for article_range in entry:
            # Determine if it's a range or single entry
            if bool(re.match(r"^[0-9]*-[0-9]*$", article_range)) is True:
                tmp = article_range.split('-')
                for i in range(int(min(tmp)), int(max(tmp)) + 1):
                    sub_list.append(i)
            else:
                # Not a range
                sub_list.append(int(article_range))

        # de-duplicate and remove bogus zeroes
        [res.append(x) for x in sub_list if x not in res and x != 0]
        return res

    # Convert article to ZipNews format
    def ZipArticle (self, article, byte_count, group_name):
        offset = 0

        # Article separator is 0x01, repeated 20 times
        buffer = (b'\x01' * 20) + b'\r\n'

        # Write index file entry for this group if there isn't already one
        if self.zip_flag is False:
            offset += len(buffer)
            idx_buffer = str.encode('N {:08} {}\r\n'.format(os.path.getsize(self.nws_filename) + offset, group_name))
            self.zip_flag = True
            with open(self.idx_filename, 'ab') as file:
                file.write(idx_buffer)

        # Maintain block count
        self.blk_cnt += (byte_count + 64) / 128

        article_unified = b''
        # re-construct message, decoding MIME and devolving unicode
        for line in article[1].lines:
            article_unified += line + b'\r\n'
            
        a_e = email.message_from_bytes(article_unified)

        # do headers
        for header, payload in a_e.items():
            if header in [ 'From', 'Subject', 'Date' ]:
                payload = unidecode(payload)
            if header == 'From':
                if type(payload) is email.header.Header:
                    payload = email.header.decode_header(payload)[0][0]
                    # this is bytes now, let's assume UTF-8
                    payload = payload.decode('utf-8')
                    # now it's (theoretically) a UTF-8 string, so carry on
                payload = unidecode(payload)
                # ZNR wants From: as "From: user@domain.tld (Pretty Name)"
                try: 
                    payload = re.sub(r'^(.*) <(.*)>$', r'\2 (\1)', payload)
                except:
                    pass
            q = '{}: {}\r\n'.format(header, payload)
            buffer += q.encode()
        buffer += b'\r\n'

        # do body
        # Do article body fixups
        for part in a_e.walk():
            if part.get_content_type() == 'text/plain':
                r = part.get_payload(decode=True)
                for line in r.splitlines():
                    # ZipNews doesn't like ^Z's
                    line.replace(b'\x1a', b' ')
                    # Attempt to convert UTF-8 to CP437
                    try:
                        line = unidecode(line.decode('utf8')).encode('CP437')
                    except:
                        pass
                    # Rewrap quoted parts if applicable
                    quote_prefix_regex = b'^(>\ ?)*'
                    quote_prefix = re.match(quote_prefix_regex, line).group()
                    if len(line) > 78:
                        wrapper = textwrap.TextWrapper(width=78, initial_indent=quote_prefix.decode(), subsequent_indent=quote_prefix.decode())
                        lines_wrapped = wrapper.wrap(text=line[len(quote_prefix):].decode(errors="ignore"))
                        line = b''
                        for line_wrapped in lines_wrapped:
                            line += line_wrapped.encode() + b'\r\n'
                    else:
                        # CR/LF at end of each line
                        line += b'\r\n'
                    # append line to buffer
                    buffer += line
        return buffer

    # Read the .newsrc file
    def ReadNewsrc(self):
        newsrc_parsed = []

        try:
            with open(self.newsrc_filename, 'r') as file:
                newsrc_raw = file.readlines()
        except Exception as e:
            eprint("uzip.py: can't open newsrc {} ({})".format(self.newsrc_filename, e))
            return None

        for newsrc_line in newsrc_raw:
            this_line = newsrc_line.rstrip('\n').split(' ')
            if this_line[0][-1] == ':':
                subscribed = True
            elif this_line[0][-1] == '!':
                subscribed = False
            if len(this_line[1]) > 0: # test for empty group
                sub_list = self.SubList(this_line[1])
            else:
                sub_list = [0, 0]
            group_name = this_line[0][0:-1]
            this_group = { 'group_name': group_name, 'subscribed': subscribed,
                'sub_list': sub_list, 'packet_article_count': 0 }
            newsrc_parsed.append(this_group)
        return newsrc_parsed

    # Write the ZipNews join file
    def WriteJoin(self, group_data):
        try:
            file = open(self.join_filename, 'wb')
        except Exception as e:
            eprint("uzip.py: can't open {} ({})".format(self.join_filename, e))
            return
        # Walk through the newsrc
        for group in group_data:
            if group['subscribed'] is True:
                if group['packet_article_count'] > 0:
                    buf = '{} {}\r\n'.format(group['group_name'], group['packet_article_count']).encode()
                    file.write(buf)
        file.close()

    # Collect unread news into a packet
    def DoNews(self):
        group_data = self.ReadNewsrc()
        if group_data is None:
            eprint('uznr.py: can\'t read newsrc file')
            return False

        self.nntp = nntplib.NNTP(self.nntp_server)
        for group in group_data:
            # Check if too many blocks already
            if (self.blk_cnt >= self.max_blks) and (self.max_blks > 0):
                # ZipNews "join" file
                self.WriteJoin(group_data)
                return True

            if group['subscribed'] is True:
                # Do this group
                ret = self.DoGroup(group)
                if ret is None:
                    eprint("uznr.py: group {} incomplete".format(group['group_name']))
                    return False
                group['sub_list'] = ret
        # ZipNews "join" file
        self.nntp.quit()
        if self.article_count > 0:
            self.WriteJoin(group_data)
            self.WriteNewsrc(group_data)
            self.DoZipfile()
            print('Zipnews Reader packet is at "{}"'.format(self.output_file))
        else:
            eprint('uznr.py: no articles done, not creating zipfile')
        rmtree(self.tmpdir)
        return True

    def WriteNewsrc (self, group_data):
        newsrc_out = b''
        for group in group_data:
            # need to handle multiple ranges, if only to error out
            for article_range in self.ranges(group['sub_list']):
                newsrc_out += '{}: {}-{}\n'.format(group['group_name'], article_range[0], article_range[1]-1).encode()
        with open(self.newsrc_filename, 'wb') as file:
            file.write(newsrc_out)

    # Process given group
    def DoGroup (self, group):
        # Lookup group in active file
        try:
            buf = self.nntp.group(group['group_name'])
        except Exception as e:
            eprint('uqwk.py: {}'.format(e))
            return None

        group_active_list = {}
        group_active_list['lo'] = buf[2]
        group_active_list['hi'] = buf[3]

        print('{} ({} - {}): '.format(group['group_name'], group_active_list['lo'], group_active_list['hi']), end='')

        # If the group doesn't exist (that is, doesn't appear in the
        # active file), do nothing else
        if group_active_list is None:
            eprint('uqwk.py: no such group {}'.format(group['group_name']))
            return None

        # Remember no ZipNews index entry yet
        self.zip_flag = False

        # Fix up the subscription list
        group['sub_list'] = [x for x in group['sub_list'] if x <= group_active_list['hi']]

        # Look through unread articles
        for i in range(group_active_list['lo'], group_active_list['hi']+1):
            # Check max block count
            if ( (self.blk_cnt >= self.max_blks) and (self.max_blks > 0) ):
                return True

            # Process this article
            if i not in group['sub_list']:
                # Process the article
                n = self.DoArticle(i, group)
                if n is True:
                    group['sub_list'].append(i)
                    group['packet_article_count'] += 1
                    print(".", end='', flush=True)
                    self.article_count += 1
                else:
                    print("e", end='', flush=True)
            else:
                print("X", end='', flush=True)

        print("")
        return sorted(group['sub_list'])

    def DoArticle(self, artnum, group):
        # retrieve article from news server
        try:
            article = self.nntp.article(artnum)
        except Exception as e:
            eprint('uznr.py: could not open {}:{} ({})'.format(group['group_name'], artnum, e))
            return False

        self.end_offset += len(article[1].lines)

        # Skip empty articles
        if self.end_offset == 0:
            eprint('uznr.py: empty article')
            return False

        # Do ZipNews stuff
        n = self.ZipArticle(article, self.end_offset, group['group_name'])
        with open(self.nws_filename, 'ab') as file:
            file.write(n)
        file.close()

        return True

    # Decrypt ZNR-encrypted data
    def transmogrify(self, incoming, key_string):
        out = bytearray()
        key = [ord(x) for x in list(key_string)]
        i = 0

        for byte in incoming:
            if type(byte) is not int:
                byte = ord(byte)
            out.append( (byte ^ key[i] ^ (key[0] * i) ) & 0xff)
            key[i] = (key[i] + key[(i + 1) % len(key)]) & 0xff
            if key[i] == 0:
                key[i] = 1
            i = (i + 1) % len(key)
        return out

    # process upload packet
    def DoUploadPacket(self, packet_filename):
        print("DoUploadPacket(): processing {}".format(packet_filename))
        with zipfile.ZipFile(packet_filename, mode='r') as myzip:
            zipfile_list = myzip.namelist()
            filtered = fnmatch.filter(zipfile_list, '*.ID')
            if len(filtered) == 0:
                eprint('DoUploadPacket(): {} does not contain an ID file'.format(packet_filename))
                return
            zipfile_id_encrypted = myzip.read(filtered[0])
            znr_good_decode = False
            for key in self.keys:
                zipfile_id = self.transmogrify(zipfile_id_encrypted, key)
                try:
                    zipfile_id = zipfile_id.decode('ascii').split('\n')
                except:
                    # a properly decoded key will convert without error
                    continue
                # packets using "uqwk" format start with "ZNR+UQWK ${SERIAL}",
                # encoded with key 'Uqwk does ZipNews. Hooray.', and native
                # packets start with 'ZNR ${SERIAL}' encoded with key
                # 'jaxsoft per bill'
                if bool(re.match(r"^ZNR(\+UQWK)? .*$", zipfile_id[0])) is True:
                    znr_good_decode = True

            if znr_good_decode is False:
                eprint("DoUploadPacket(): could not decode {}".format(packet_filename))
                return

            # ... now here's a question: do we actually care about the contents
            # of the .ID file?  The username is going to be whatever ZNR thinks
            # is the account name (which most likely isn't going to match
            # the actual email account name), and the domain similarly wrong.
            #
            # However, having this check present allows us to test for ZNR
            # configuration validity (and having an implementation in UQWK
            # gave us the algorithm by which the registration key was encoded),
            # so we'll leave it in and override as desired.
            if self.from_address is not None and self.from_gecos is not None:
                znr_username = self.from_gecos
                znr_login = self.from_address
            else:
                znr_username = zipfile_id[1]
                znr_login = "{}@{}".format(zipfile_id[2], self.host_name)
            znr_from_header = '"{}" <{}>'.format(znr_username, znr_login)

            # Send mail packets ("^*.M[0-9]*$")
            for element in fnmatch.filter(zipfile_list, '*.M??'):
                mail_raw = myzip.read(element)
                mail = email.message_from_bytes(mail_raw)
                mail.add_header('From', znr_from_header)
                try:
                    s = smtplib.SMTP(self.smtp_server)
                    s.send_message(mail)
                    s.quit()
                except Exception as e:
                    eprint("DoUploadPacket(): could not send email message via SMTP server {} ({})".format(self.smtp_server, e))
                eprint("DoUploadPacket(): sent {}".format(element))

            # Send news packets ("^*.N[0-9]*$")
            for element in fnmatch.filter(zipfile_list, '*.N??'):
                article_raw = myzip.read(element)
                article = email.message_from_bytes(article_raw)
                article.add_header('From', znr_from_header)
                try:
                    self.nntp = nntplib.NNTP(self.nntp_server)
                except Exception as e:
                    eprint("DoUploadPacket(): could not connect to NNTP server {} ({})".format(self.nntp_server, e))
                    return
                eprint("DoUploadPacket(): sending {}".format(element))
                try:
                    self.nntp.post(article.as_bytes())
                except Exception as e:
                    eprint("DoUploadPacket(): error '{}'".format(e))
                self.nntp.quit()

def main():
    parser = argparse.ArgumentParser(
        prog='uznr.py',
        description='USENET packet conversion utility for ZipNews Reader',
        epilog='Written by Chris RYU (https://codeberg.org/cryu/zipnews_reader)'
        )

    parser.add_argument('-%', '--debug', action='store_true', dest='do_debug', help='enable debug', default=False)
    parser.add_argument('-A', '--auth', type=str, dest='do_auth', help='userid:password (default None)', default=None)
    parser.add_argument('-o', '--output', type=str, dest='output_file', help='output file (default ${PWD}/${USERID}.zip)', default=os.environ['PWD']+'/'+os.environ['USER']+'.zip')
    parser.add_argument('-p', '--packet', type=str, dest='packet_fn', help='upload packet path (default None)', default=None)
    parser.add_argument('-f', '--from', type=str, dest='from_address', help='user-defined from address (format "userid@domain", default None)', default=None)
    parser.add_argument('-g', '--gecos', type=str, dest='from_gecos', help='user-defined GECOS (format "This Is My Name", default None)', default=None)
    parser.add_argument('-N', '--nntp-server', type=str, dest='nntp_server', help='NNTP server (default "localhost")', default="localhost")
    parser.add_argument('-S', '--smtp-server', type=str, dest='smtp_server', help='SMTP server (default "localhost")', default="localhost")
    parser.add_argument('-m', '--mail', action='store_true', dest='do_mail', help='do mail (default False)', default=False)
    parser.add_argument('-n', '--news', action='store_true', dest='do_news', help='do news (default False)', default=False)
    parser.add_argument('-B', '--max-blocks', type=int, dest='max_blks', help='max blocks (default 4096)', default=4096)
    args = parser.parse_args()

    znr = ZNR(smtp_server=args.smtp_server, nntp_server=args.nntp_server, from_address=args.from_address, from_gecos=args.from_gecos, output_file=args.output_file)

    if args.packet_fn is not None:
        znr.DoUploadPacket(args.packet_fn)

    if args.do_news:
        print("doing news")
        znr.DoNews()

    if args.do_mail:
        print("do mail here")

    # close things up


if __name__=='__main__':
    main()
